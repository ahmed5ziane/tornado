import tornado.web
import tornado.websocket
import asyncio


clients = set()


class MainHandler(tornado.web.RequestHandler):

    def get(self):

        self.render("ui/template/index.html")
        return True


class SocketHandler(tornado.websocket.WebSocketHandler):

    def open(self):
        print("WebSocket opened")

    def on_message(self, message):
        self.write_message(u"You said: " + message)

    def on_close(self):
        print("WebSocket closed")


async def main():
    app = tornado.web.Application([
        (r"/?", MainHandler),
        (r"/ws/?", SocketHandler),
    ])
    app.listen(3210)

    shutdown_event = asyncio.Event()
    await shutdown_event.wait()


if __name__ == "__main__":
    asyncio.run(main;
